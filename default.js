//$(function(){ todo codigo é escrito aqui dentro, força o programa executar o código só depois que a pagina estiver carregada (DOM).
    //document.getElementById("botao");
    //document.getElementByClassName"botao");
    //document.getElementByTagName("h1");
    // a funcao abaixo faz a mesma coisa do getelement acima

$(function() {

    var segundo = 0, minuto = 0, hora = 0;
    var ss, mm, hh;
    var cronometro = null;

    function incrementaContador() {
        segundo++
        if (segundo === 60) {
            minuto++;
            segundo = 0;
        }
        if (minuto === 60) {
            hora++;
            minuto = 0;
        }
        ss = (segundo < 10)? "0" + segundo : segundo;
        mm = (minuto < 10)? "0" + minuto : minuto;
        hh = (hora < 10)? "0" + hora : hora;

        $("#contador").text(hh + ":" + mm + ":" + ss); // # é uma seletor de id, vai imprimir hh:mm:ss
        }

        $("#btn-iniciar").on('click', function() {
            if (cronometro === null) {
                cronometro = setInterval(incrementaContador, 1000);
                $("#btn-iniciar").text("PAUSAR");
            } else {
                clearInterval(cronometro);
                cronometro = null;
                $("#btn-iniciar").text("RETOMAR");
            }
        });
        $("#btn-zerar").on('click', function (){ // quando clicado no botao de zerar
            clearInterval(cronometro); // limpa o intervalo de contagem, para o contador
            cronometro = null; // define o cronometro como vazio
            segundo = minuto = hora = 0; // define o contador como zero
            $("#contador").text("00:00:00"); // colocando zero no contador do html (visivel ao usuario)
            $("#btn-iniciar").text("INICIAR");  // alterando o texto do botao para iniciar
        })

    });
